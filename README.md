# Packer WSM 2019

Packer AMI for WorkSpaces Manager with all software requirements


## Variable: AWS Access and Secret Keys
In this case, we are using a variable for the Access Key and the Secret Key for the AWS API via the GitLab CI/CD inherited variables or the GitLab Runner EC2 Instance Profile:
```yaml
variable "access_token" {
  type    = string
  default = "${env("AWS_ACCESS_KEY_ID")}"
}
```

```yaml
variable "secret_token" {
  type    = string
  default = "${env("AWS_SECRET_ACCESS_KEY")}"
}
```


## Variable: Region
The region is set on variable:
```yaml
variable "region" {
  type    = string
  default = "eu-central-1"
}
```

You can read about regions on this link: https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/using-regions-availability-zones.html

This is also another variable related to the AWS regions in which the AMI will be copied to as:
```yaml
variable "ami_region" {
  type = list(string)
  default = ["eu-central-1"]
}
```


## Variable: AWS Account
The AWS Account to which the AMI will be copied is identified as the variable `ami_user`:
```yaml
variable "ami_user" {
  type = list(string)
  default = ["010101010101"]
}
```


## Variable: Security Group
Security Group is set on variable:
```yaml
variable "sg_id" {
  type    = string
  default = "sg-11111aa11a1111a11"
}
```

The Security Group must be created in advance, prior to running the Packer pipeline. This is an example:
* Security Group ID: sg-11111aa11a1111a11
* Security Group Name: SG-PACKER

This needs to be changed according to each deployment.

It contains the following inbound rules:
* TCP/22 from 0.0.0.0/0 (when using Linux for SSH)
* TCP/5986 from 0.0.0.0/0 (when using Windows for WinRM-HTTPS)


## Variable: Networking
There are two main Networking variables for VPC and Subnet. It is important to mention that the Packer instance must have a public IP at the moment of creation. Variables for VPC is:
```yaml
variable "vpc_id" {
  type    = string
  default = "vpc-11a11a1a"
}
```

And for Subnet is:
```yaml
variable "subnet_id" {
  type    = string
  default = "subnet-a11a1aaa"
}
```

* Make sure it is on a subnet with a public IP assigned automatically


## Variable: AMI search
While this example uses a default image of Windows Server 2019 because of this part:
```yaml
data "amazon-ami" "aws_ami" {
  filters = {
    name                = "Windows_Server-2019-English-Full-Base-*"
    root-device-type    = "ebs"
    virtualization-type = "hvm"
  }
  most_recent = true
  owners      = ["amazon"]
  region      = var.region
}
```

In order to use a different version of Windows, per example 2022, we just need to change the `name` filter to `"Windows_Server-2022-English-Full-Base-*"`. Per example:
```yaml
data "amazon-ami" "aws_ami" {
  filters = {
    name                = "Windows_Server-2022-English-Full-Base-*"
    root-device-type    = "ebs"
    virtualization-type = "hvm"
  }
  most_recent = true
  owners      = ["amazon"]
  region      = var.region
}
```
