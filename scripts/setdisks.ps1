Write-Host "Formatting D Drive"
Get-Disk |
Where partitionstyle -eq 'raw' |
Initialize-Disk -PartitionStyle MBR -PassThru |
New-Partition -AssignDriveLetter -UseMaximumSize |
Format-Volume -FileSystem NTFS -NewFileSystemLabel "PORTAL" -Confirm:$false

Write-Host "Labelling disks..."
Set-Volume -DriveLetter C -NewFileSystemLabel "SYSTEM"
Set-Volume -DriveLetter D -NewFileSystemLabel "PORTAL"
