rem Nuvens Scheduler is no longer a requirement since 5.5.0
rem These lines will be removed on next release
rem echo Installing Nuvens Scheduler...
rem "D:\Deploy\Nuvens Portal Scheduler.msi" /qn /norestart

echo Installing MS SQL Management Studio...
powershell -NoProfile -ExecutionPolicy unrestricted -Command "(New-Object System.Net.WebClient).DownloadFile('https://aka.ms/ssmsfullsetup?clcid=0x409', 'C:\Windows\Temp\SSMS-Setup-ENU.exe')" <NUL
C:\Windows\Temp\SSMS-Setup-ENU.exe /install /quiet /norestart
