echo Download Microsoft System CLR Types for SQL Server
powershell -NoProfile -ExecutionPolicy unrestricted -Command "(New-Object System.Net.WebClient).DownloadFile('https://gitlab.com/api/v4/projects/36702690/repository/files/SQLSysClrTypes.msi/raw?ref=main', 'C:\Windows\Temp\SQLSysClrTypes.msi')" <NUL

echo Download VC Redistributable
powershell -NoProfile -ExecutionPolicy unrestricted -Command "(New-Object System.Net.WebClient).DownloadFile('https://gitlab.com/api/v4/projects/36702690/repository/files/vcredist_x86.exe/raw?ref=main', 'C:\Windows\Temp\vcredist_x86.exe')" <NUL
powershell -NoProfile -ExecutionPolicy unrestricted -Command "(New-Object System.Net.WebClient).DownloadFile('https://gitlab.com/api/v4/projects/36702690/repository/files/vcredist_x64.exe/raw?ref=main', 'C:\Windows\Temp\vcredist_x64.exe')" <NUL
powershell -NoProfile -ExecutionPolicy unrestricted -Command "(New-Object System.Net.WebClient).DownloadFile('https://gitlab.com/api/v4/projects/36702690/repository/files/VC_redist.x86.exe/raw?ref=main', 'C:\Windows\Temp\VC_redist.x86.exe')" <NUL
powershell -NoProfile -ExecutionPolicy unrestricted -Command "(New-Object System.Net.WebClient).DownloadFile('https://gitlab.com/api/v4/projects/36702690/repository/files/VC_redist.x64.exe/raw?ref=main', 'C:\Windows\Temp\VC_redist.x64.exe')" <NUL

echo Download Microsoft Access Database Engine 2016 Redistributable
powershell -NoProfile -ExecutionPolicy unrestricted -Command "(New-Object System.Net.WebClient).DownloadFile('https://gitlab.com/api/v4/projects/36702690/repository/files/accessdatabaseengine_X64.exe/raw?ref=main', 'C:\Windows\Temp\accessdatabaseengine_X64.exe')" <NUL

echo Download Microsoft URL Rewrite Module 2.1
powershell -NoProfile -ExecutionPolicy unrestricted -Command "(New-Object System.Net.WebClient).DownloadFile('https://gitlab.com/api/v4/projects/36702690/repository/files/rewrite_amd64_en-US.msi/raw?ref=main', 'C:\Windows\Temp\rewrite_amd64_en-US.msi')" <NUL

echo Download Microsoft .NET Framework 4.8 offline installer
powershell -NoProfile -ExecutionPolicy unrestricted -Command "(New-Object System.Net.WebClient).DownloadFile('https://gitlab.com/api/v4/projects/36702690/repository/files/ndp48-x86-x64-allos-enu.exe/raw?ref=main', 'C:\Windows\Temp\ndp48-x86-x64-allos-enu.exe')" <NUL

echo Download Microsoft .NET Core 5
powershell -NoProfile -ExecutionPolicy unrestricted -Command "(New-Object System.Net.WebClient).DownloadFile('https://gitlab.com/api/v4/projects/36702690/repository/files/dotnet-hosting-5.0.15-win.exe/raw?ref=main', 'C:\Windows\Temp\dotnet-hosting-5.0.15-win.exe')" <NUL

echo Download Microsoft Web Deploy
powershell -NoProfile -ExecutionPolicy unrestricted -Command "(New-Object System.Net.WebClient).DownloadFile('https://gitlab.com/api/v4/projects/36702690/repository/files/WebDeploy_amd64_en-US.msi/raw?ref=main', 'C:\Windows\Temp\WebDeploy_amd64_en-US.msi')" <NUL

echo Download WorkSpaces Manager App
powershell -NoProfile -ExecutionPolicy unrestricted -Command "(New-Object System.Net.WebClient).DownloadFile('https://nuvensworkspacesmanager.s3.eu-west-1.amazonaws.com/latest/WSM.zip', 'C:\Windows\Temp\WSM.zip')" <NUL

echo Download WorkSpaces Manager DB Init
powershell -NoProfile -ExecutionPolicy unrestricted -Command "(New-Object System.Net.WebClient).DownloadFile('https://gitlab.com/api/v4/projects/36702690/repository/files/Initialise_Portal.sql/raw?ref=main', 'C:\Windows\Temp\Initialise_Portal.sql')" <NUL

echo Download Registry File for WSM
powershell -NoProfile -ExecutionPolicy unrestricted -Command "(New-Object System.Net.WebClient).DownloadFile('https://gitlab.com/api/v4/projects/36702690/repository/files/nuvens.reg/raw?ref=main', 'C:\Windows\Temp\nuvens.reg')" <NUL
