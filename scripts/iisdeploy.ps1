Write-Host "Unzipping Deployment Files..."
Expand-Archive -LiteralPath C:\Windows\Temp\WSM.zip -DestinationPath D:\Deploy

Write-Host "Create Portal folder..."
New-Item -Path "D:\" -Name "Portal" -ItemType "directory"

Write-Host "Delete default IIS WebSite..."
Remove-IISSite -Name "Default Web Site" -Confirm:$false
<# Remove-WebAppPool -Name "DefaultAppPool" -Confirm:$false #>

Write-Host "Create Portal Application..."
New-IISSite -Name "Portal" -BindingInformation "*:80:" -PhysicalPath "D:\Portal"

Write-Host "Copying Registry File for WSM Client..."
Copy-Item "C:\Windows\Temp\nuvens.reg" -Destination "D:\Deploy"

Write-Host "Deploying Portal..."
D:\Deploy\Portal.deploy.cmd /Y

Write-Host "Initializing Portal..."
iisreset
