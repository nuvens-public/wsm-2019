echo Installing VC Redistributable...
cmd /c "C:\Windows\Temp\vcredist_x86.exe /Q"
cmd /c "C:\Windows\Temp\vcredist_x64.exe /Q"
cmd /c "C:\Windows\Temp\VC_redist.x86.exe /Q"
cmd /c "C:\Windows\Temp\VC_redist.x64.exe /Q"

echo Installing Microsoft Access Database Engine 2016 Redistributable...
cmd /c "C:\Windows\Temp\accessdatabaseengine_X64.exe /quiet"

echo Installing Microsoft .NET Framework 4.8 offline...
cmd /c "C:\Windows\Temp\ndp48-x86-x64-allos-enu.exe /q /norestart"

echo Installing Microsoft .NET Core 5...
cmd /c "C:\Windows\Temp\dotnet-hosting-5.0.15-win.exe /install /quiet /norestart"

echo Installing Microsoft URL Rewrite Module 2.1...
C:\Windows\Temp\rewrite_amd64_en-US.msi /qn

echo Installing Microsoft System CLR Types for SQL Server...
C:\Windows\Temp\SQLSysClrTypes.msi /qn /log "%temp%\MSSystemCLRTypesSQL.log" /norestart ALLUSERS=2
rem msiexec /i /qn C:\Windows\Temp\SQLSysClrTypes.msi /L* "%temp%\CLR Types for SQL Server.log" /norestart ALLUSERS=2

echo Installing Microsoft Web Deploy...
C:\Windows\Temp\WebDeploy_amd64_en-US.msi ADDLOCAL=ALL /qn /norestart LicenseAccepted="0"
