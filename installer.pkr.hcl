variable "access_token" {
  type    = string
  default = "${env("AWS_ACCESS_KEY_ID")}"
}

variable "secret_token" {
  type    = string
  default = "${env("AWS_SECRET_ACCESS_KEY")}"
}

variable "region" {
  type    = string
  default = "eu-central-1"
}

variable "source_version" {
  type    = string
  default = "5.3.2"
}

variable "ins_type" {
  type    = string
  default = "t3a.medium"
}

variable "vpc_id" {
  type    = string
  default = "vpc-11a11a1a"
}

variable "subnet_id" {
  type    = string
  default = "subnet-a11a1aaa"
}

variable "sg_id" {
  type    = string
  default = "sg-11111aa11a1111a11"
}

variable "ssh_user" {
  type    = string
  default = "ec2-user"
}

variable "win_user" {
  type    = string
  default = "Administrator"
}

variable "user_profile" {
  type    = string
  default = "default"
}

variable "ami_region" {
  type = list(string)
  default = ["eu-central-1"]
}

variable "ami_user" {
  type = list(string)
  default = ["010101010101"]
}

locals { timestamp = regex_replace(timestamp(), "[- TZ:]", "") }

data "amazon-ami" "aws_ami" {
  filters = {
    name                = "Windows_Server-2019-English-Full-Base-*"
    root-device-type    = "ebs"
    virtualization-type = "hvm"
  }
  most_recent = true
  owners      = ["amazon"]
  region      = var.region
}

source "amazon-ebs" "aws_ebs" {
#  ami_block_device_mappings {
  launch_block_device_mappings {
    device_name           = "/dev/sda1"
    volume_size           = 80
    volume_type           = "gp3"
    iops                  = 3000
    throughput            = 125
    delete_on_termination = true
  }
  launch_block_device_mappings {
    device_name           = "xvdb"
    volume_size           = 20
    volume_type           = "gp3"
    iops                  = 3000
    throughput            = 125
    delete_on_termination = true
  }
  ami_description       = "EC2 AMI for WorkSpaces Manager by Nuvens-${timestamp()}"
  ami_name              = "WSM-2019-WorkSpaces-Manager-${local.timestamp}"
  ami_regions           = var.ami_region
  ami_users             = var.ami_user
  force_delete_snapshot = true
  force_deregister      = true
  instance_type         = var.ins_type
  region                = var.region
  access_key            = var.access_token
  secret_key            = var.secret_token
  profile               = var.user_profile
  source_ami            = "${data.amazon-ami.aws_ami.id}"
  user_data_file        = "boot_config/winrm_bootstrap.ps1"
  communicator          = "winrm"
  winrm_username        = var.win_user
  winrm_insecure        = true
  winrm_use_ssl         = true
  vpc_id                = var.vpc_id
  subnet_id             = var.subnet_id
  security_group_id     = var.sg_id
  tags = {
    Name          = "EC2 AMI for WorkSpaces Manager by Nuvens-${timestamp()}"
    OS_Version    = "Windows 2019 for WorkSpaces Manager"
    Release       = var.source_version
    Packer        = "True"
    Base_AMI_ID   = "{{ .SourceAMI }}"
    Base_AMI_Name = "{{ .SourceAMIName }}"
  }
}

build {
  sources = ["source.amazon-ebs.aws_ebs"]

  provisioner "windows-shell" {
    inline = ["echo Connected via WinRM at '${build.User}@${build.Host}:${build.Port}'"]
  }

  provisioner "powershell" {
	scripts = ["./scripts/setdisks.ps1"]
  }

  provisioner "windows-shell" {
    scripts = ["./scripts/downloads.bat"]
  }

  provisioner "powershell" {
    inline = [
      "Install-WindowsFeature -name Web-Server -IncludeManagementTools",
      "dism /online /enable-feature /all /featurename:IIS-ASPNET45",
      "Enable-WindowsOptionalFeature -Online -FeatureName IIS-HttpRedirect",
      "Enable-WindowsOptionalFeature -Online -FeatureName IIS-ISAPIExtensions",
      "Enable-WindowsOptionalFeature -Online -FeatureName IIS-ISAPIFilter",
      "Enable-WindowsOptionalFeature -Online -FeatureName IIS-WebSockets",
      "Enable-WindowsOptionalFeature -Online -FeatureName IIS-WindowsAuthentication"
    ]
  }

  provisioner "powershell" {
    scripts = ["./scripts/disable-uac.ps1"]
  }

  provisioner "windows-shell" {
    scripts = ["./scripts/enable-rdp.bat"]
  }

  provisioner "windows-shell" {
    scripts = ["./scripts/chocolatey.bat"]
  }

  provisioner "windows-shell" {
    scripts = ["./scripts/installnet4.bat"]
  }

  provisioner "windows-shell" {
    scripts = ["./scripts/wsmfiles.bat"]
  }

  provisioner "powershell" {
    scripts = ["./scripts/iisdeploy.ps1"]
  }

  provisioner "windows-shell" {
    scripts = ["./scripts/sqlstudio.bat"]
  }

  provisioner "powershell" {
    inline = [
      "C:\\ProgramData\\Amazon\\EC2-Windows\\Launch\\Scripts\\SendWindowsIsReady.ps1 -Schedule",
      "C:\\ProgramData\\Amazon\\EC2-Windows\\Launch\\Scripts\\InitializeInstance.ps1 -Schedule",
      "C:\\ProgramData\\Amazon\\EC2-Windows\\Launch\\Scripts\\SysprepInstance.ps1 -NoShutdown"
	  ]
  }
}
